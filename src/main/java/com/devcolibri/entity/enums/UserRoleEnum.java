package com.devcolibri.entity.enums;

/**
 * Created by y.voytovich on 09.09.2015.
 */
public enum UserRoleEnum {

    ADMIN,
    USER,
    ANONYMOUS;

    UserRoleEnum() {
    }

}