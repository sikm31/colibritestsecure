package com.devcolibri.service;

import com.devcolibri.entity.User;

/**
 * Created by y.voytovich on 09.09.2015.
 */
public interface UserService {

    User getUser(String login);

}